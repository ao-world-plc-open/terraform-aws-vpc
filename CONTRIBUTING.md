# Pull Request Process

1. Ensure all `variables` have a type and a description
2. Ensure all `outputs` have a description, and that the type is also prepended to the description
   in markdown italics e.g. "*list(string)* Your output description goes here"
3. Ensure you have run `terraform fmt` on your code
4. Ensure that documentation has been regenerated with `terraform-docs .`
5. Update the CHANGELOG.md with details of changes to the modules, or any new module additions.
6. Increase the version number in CHANGELOG.md to the new version that this Pull Request would
   represent. The versioning scheme we use is [SemVer](http://semver.org/).
7. Push your changes on a new branch and submit a merge request. Once the merge request has been
   reviewed we will either feedback with suggestions/comments or merge the change.
