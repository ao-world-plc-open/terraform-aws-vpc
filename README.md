# Terraform Module: AWS VPC
This module allows you to deploy an AWS VPC in your account to AO standard. 
It will create a VPC, private and public subnets, and required route tables.
If you specify public subnets an internet gateway will be created, and the
appropriate routes will be added.

* [Example Usage](#example-usage)
  * [Basic](#basic)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Basic
You should be able to use this example as is for the majority of use cases
by updating the following inputs:
  * customer: Your team name, e.g. devops
  * envtype: The environment that will use the VPC e.g. beta, staging, or
    prod
  * vpc_cidr: A CIDR IP range to use in your VPC, ensure you consult with 
    your network team before picking this

This example deploys our recommended VPC layout, including using the
Terraform `cidrsubnet` function to automatically create subnets with the 
minimum of waste:
  * 3 Public Subnets
  * 3 Private Subnets
  * An Internet Gateway

```hcl
module "vpc" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-vpc.git?ref=v2.1.0"

  customer        = "devops"
  envtype         = "demo"
  private_subnets = [for i in range(0, 3) : cidrsubnet("172.16.0.0/24", 2, i)]
  public_subnets  = [for i in range(12, 15) : cidrsubnet("172.16.0.0/24", 4, i)]
  vpc_cidr        = "172.16.0.0/24"
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 3.26.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| customer | The Team the resource belongs to, e.g. devops | `string` | n/a | yes |
| envtype | The type of environment being deployed, e.g. beta, staging, or live | `string` | n/a | yes |
| private\_subnets | A list of private subnets CIDRs inside the VPC, within the CIDR range specified in vpc\_cidr. It must have the same amount of elements as azs and public\_subnets | `list(string)` | n/a | yes |
| public\_subnets | A list of private subnets CIDRs inside the VPC, within the CIDR range specified in vpc\_cidr. It must have the same amount of elements as azs and private\_subnets | `list(string)` | n/a | yes |
| vpc\_cidr | The super CIDR range assigned to the VPC. This must be provided by Infra Networks to allow the VPC to be connected to Transit | `string` | n/a | yes |
| aws\_region | The AWS region the VPC is being deployed into | `string` | `null` | no |
| azs | A list of Availability zones in the region, it must have the same amount of elements as public\_subnets and private\_subnets | `list(string)` | `[]` | no |
| custom\_tags | Custom tags to apply to the module resources. Any tag supplied that matches the name of a tag set by the module will take precedence | `map(string)` | `{}` | no |
| domain\_name | The primary domain name for DNS Search | `string` | `null` | no |
| domain\_name\_servers | DNS Servers assigned via DHCP for instances to use inside the VPC | `list(string)` | ```[ "AmazonProvidedDNS" ]``` | no |
| enable\_dns\_hostnames | Allow the VPC to generate public DNS records for instances with public IPs | `bool` | `false` | no |
| enable\_dns\_support | Enable the VPC DNS resolvers used for AmazonProvidedDNS | `bool` | `true` | no |
| envname | The name of the environment being deployed, we recommend vpc unless you're deploying an application per VPC | `string` | `"vpc"` | no |
| map\_public\_ip\_on\_launch | Enable instances created in public subnets to automatically have a public IP assigned | `bool` | `true` | no |

## Outputs

| Name | Description |
|------|-------------|
| availability\_zones | *list(string)* Availability zones (AZs) in use in the VPC |
| private\_route\_tables | *list(string)* Private route table IDs |
| private\_subnets | *list(string)* Private subnet IDs |
| public\_route\_tables | *list(string)* Public route table IDs |
| public\_subnets | *list(string)* Public subnet IDs |
| vpc\_cidr | *string* Super CIDR assigned to the VPC |
| vpc\_id | *string* The VPC ID |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.