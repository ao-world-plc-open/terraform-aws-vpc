module "vpc" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-vpc.git?ref=v2.1.0"

  customer        = "devops"
  envtype         = "demo"
  private_subnets = [for i in range(0, 3) : cidrsubnet("172.16.0.0/24", 2, i)]
  public_subnets  = [for i in range(12, 15) : cidrsubnet("172.16.0.0/24", 4, i)]
  vpc_cidr        = "172.16.0.0/24"
}