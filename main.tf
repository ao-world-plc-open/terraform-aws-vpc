locals {
  azs        = length(var.azs) > 0 ? var.azs : data.aws_availability_zones.available.names
  aws_region = var.aws_region != null ? var.aws_region : data.aws_region.current.name
  tags = merge({
    Environment = var.envname
    EnvType     = var.envtype
    Name        = "${var.customer}-${var.envname}-${var.envtype}"
  }, var.custom_tags)
}

resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support
  tags                 = local.tags
}

resource "aws_vpc_dhcp_options" "vpc" {
  domain_name         = var.domain_name
  domain_name_servers = var.domain_name_servers
  tags                = local.tags
}

resource "aws_vpc_dhcp_options_association" "vpc_dhcp" {
  dhcp_options_id = aws_vpc_dhcp_options.vpc.id
  vpc_id          = aws_vpc.vpc.id
}

# Ensure the default security group created for the VPC has no rules attached
# so it doesn't generate audit alarms, and enforce it if someone changes it
resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.vpc.id

  ingress = []
  egress  = []
}

### Public Subnets
resource "aws_subnet" "public" {
  count = length(var.public_subnets)

  availability_zone       = local.azs[count.index]
  cidr_block              = var.public_subnets[count.index]
  map_public_ip_on_launch = var.map_public_ip_on_launch
  tags = merge(local.tags, {
    Name = "${local.tags.Name}-public-${local.azs[count.index]}"
  })
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table" "public" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  tags = merge(local.tags, {
    Name = "${local.tags.Name}-public"
  })
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table_association" "public" {
  count = length(var.public_subnets)

  route_table_id = aws_route_table.public[0].id
  subnet_id      = aws_subnet.public[count.index].id
}

resource "aws_internet_gateway" "igw" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  vpc_id = aws_vpc.vpc.id
  tags   = local.tags
}

resource "aws_route" "public_internet_gateway" {
  count = length(var.public_subnets) > 0 ? 1 : 0

  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw[0].id
  route_table_id         = aws_route_table.public[0].id
}


### Private Subnets
resource "aws_subnet" "private" {
  count = length(var.private_subnets)

  availability_zone = local.azs[count.index]
  cidr_block        = var.private_subnets[count.index]
  tags = merge(local.tags, {
    Name = "${local.tags.Name}-private-${local.azs[count.index]}"
  })
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table" "private" {
  count = length(var.private_subnets)

  tags = merge(local.tags, {
    Name = "${local.tags.Name}-private-${local.azs[count.index]}"
  })
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route_table_association" "private" {
  count = length(var.private_subnets)

  route_table_id = aws_route_table.private[count.index].id
  subnet_id      = aws_subnet.private[count.index].id
}
