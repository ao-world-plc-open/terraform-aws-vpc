# Change Log

## v2.1.0

* Added .terraform-doc.yml to auto generate (mostly) README.md
* Added basic example config for README.md
* All variables and outputs have descriptions and types
* Trimmed down the amount of required variables to the minimum. However,
  the module is still backwards compatible as the old inputs exist as
  overrides
* Ran `terraform fmt` on the repository
* Updated CONTRIBUTING.md to cover running fmt and ensuring docs are
  regenerated
* Added .gitlab-ci.yml to do basic checks for fmt and out of date docs

## v2.0.2

* Adds control of the default security group to the module and defaults it to having no inbound or outbound rules.

## v2.0.1

* Cleaning up [] from wrapping the modules outputs as it was old syntax
  and now causing lists to be nested


## v2.0.1

* Cleaning up [] from wrapping the modules outputs as it was old syntax and now
  causing lists to be nested

## v2.0.0

* Added minimum provider version constraints per Terraform best practice
* Confirmed terraform validate passing for terraform 0.13, 0.14, and 0.15
* Minimum aws provider version set to where support for .aws/sso/cache is enabled
* Terraform 0.13 now the minimum version due to use of required_providers syntax

## v1.0.0

### Features

Upgraded module to TF 12.19 with provider 2.64

### Fixes

*N/A*

## v0.3.0

### Features

* Made internet gateways and routes optional if there are no public subnets. 

### Fixes

*N/A*

## v0.2.0

### Features

* Adding envtype variable to name tags so VPC resources can be easily distinguished. 

### Fixes

*N/A*

## v0.1.0

### Features

* Initial Commit

### Fixes

*N/A*
