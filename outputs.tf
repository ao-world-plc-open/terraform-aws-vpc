output "availability_zones" {
  description = "*list(string)* Availability zones (AZs) in use in the VPC"
  value       = aws_subnet.public.*.availability_zone
}

output "public_route_tables" {
  description = "*list(string)* Public route table IDs"
  value       = aws_route_table.public.*.id
}

output "public_subnets" {
  description = "*list(string)* Public subnet IDs"
  value       = aws_subnet.public.*.id
}

output "private_route_tables" {
  description = "*list(string)* Private route table IDs"
  value       = aws_route_table.private.*.id
}

output "private_subnets" {
  description = "*list(string)* Private subnet IDs"
  value       = aws_subnet.private.*.id
}

output "vpc_cidr" {
  description = "*string* Super CIDR assigned to the VPC"
  value       = aws_vpc.vpc.cidr_block
}

output "vpc_id" {
  description = "*string* The VPC ID"
  value       = aws_vpc.vpc.id
}
