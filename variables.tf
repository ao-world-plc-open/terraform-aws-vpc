variable "aws_region" {
  description = "The AWS region the VPC is being deployed into"
  type        = string
  default     = null
}

variable "azs" {
  description = "A list of Availability zones in the region, it must have the same amount of elements as public_subnets and private_subnets"
  type        = list(string)
  default     = []
}

variable "customer" {
  description = "The Team the resource belongs to, e.g. devops"
  type        = string
}

variable "custom_tags" {
  description = "Custom tags to apply to the module resources. Any tag supplied that matches the name of a tag set by the module will take precedence"
  type        = map(string)
  default     = {}
}

variable "domain_name" {
  description = "The primary domain name for DNS Search"
  type        = string
  default     = null
}

variable "domain_name_servers" {
  description = "DNS Servers assigned via DHCP for instances to use inside the VPC"
  type        = list(string)
  default     = ["AmazonProvidedDNS"]

}

variable "enable_dns_hostnames" {
  description = "Allow the VPC to generate public DNS records for instances with public IPs"
  type        = bool
  default     = false
}

variable "enable_dns_support" {
  description = "Enable the VPC DNS resolvers used for AmazonProvidedDNS"
  type        = bool
  default     = true
}

variable "envname" {
  description = "The name of the environment being deployed, we recommend vpc unless you're deploying an application per VPC"
  type        = string
  default     = "vpc"
}

variable "envtype" {
  description = "The type of environment being deployed, e.g. beta, staging, or live"
  type        = string
}

variable "map_public_ip_on_launch" {
  description = "Enable instances created in public subnets to automatically have a public IP assigned"
  type        = bool
  default     = true
}

variable "private_subnets" {
  description = "A list of private subnets CIDRs inside the VPC, within the CIDR range specified in vpc_cidr. It must have the same amount of elements as azs and public_subnets"
  type        = list(string)
}

variable "public_subnets" {
  description = "A list of private subnets CIDRs inside the VPC, within the CIDR range specified in vpc_cidr. It must have the same amount of elements as azs and private_subnets"
  type        = list(string)
}

variable "vpc_cidr" {
  description = "The super CIDR range assigned to the VPC. This must be provided by Infra Networks to allow the VPC to be connected to Transit"
  type        = string
}
